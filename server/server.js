import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import { readdirSync } from "fs";

const morgan = require("morgan");
require("dotenv").config();

const app = express();

//middlewire

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  cors({
    origin: ["http://localhost:3000"],
  })
);
app.use(morgan("dev"));

// auto load route
readdirSync("./routes").map((r) => app.use("/api", require(`./routes/${r}`)));

//db
const MONGODB_URI = `mongodb+srv://${process.env.DB_ADMIN}:${process.env.DB_PASSWORD}@cluster0.cunbo.mongodb.net/social-project-ud`;
const PORT = process.env.PORT || 8080;
mongoose
  .connect(MONGODB_URI)
  .then(() => {
    app.listen(PORT, () => {
      console.log("database connected");
      console.log(`Server is listening on Port ${PORT}`);
    });
  })
  .catch((e) => console.log(e));
