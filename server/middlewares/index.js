import { expressjwt } from "express-jwt";
import Post from '../models/post'

export const requireSignin = expressjwt({
  secret: process.env.JWT_SECRET,
  algorithms: ["HS256"],
});

export const canEditDeletePost = async (req, res, next) => {
  try{
    // const post = await Post.findById(req.params._id)
    const post = await Post.findOne({
      _id: req.params._id,
      postedBy: req.auth._id
    })
    console.log(post)
    if(!post) {
      return res.status(400).send("unauthorized")
    } else {
      next()
    }
    // if(req.auth._id != post.postedBy) {
    //   return res.status(400).send("unauthorized")
    // } else {
    //   next()
    // }
  } catch (e) {
    console.log(e)
  }
}
